import { Component } from '@angular/core';
import { WebSocketAPI } from './services/WebSocketAPI';
import {Message} from './models/message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular8-springboot-websocket';

  webSocketAPI: WebSocketAPI;
  greeting: any;
  name: string;
  message: Message;

  ngOnInit() {
    this.webSocketAPI = new WebSocketAPI(new AppComponent());
    this.message = new Message();
  }

  connect(){
    this.webSocketAPI._connect();
  }

  disconnect(){
    this.webSocketAPI._disconnect();
  }

  sendMessage(type: string){
    this.message.searchType = type;
    this.message.content = this.name;
    this.webSocketAPI._send(this.message);
  }

  sendMessage2(type: string){

    let m = new Message();
    m.searchType = "BusinessType";
    m.content = "shinomantazh";

    this.message.searchType = type;
    this.message.content = this.name;
    this.message.inputDTO = m;
    this.webSocketAPI._send(this.message);
  }

  handleMessage(message){
    this.greeting = message;
  }
}
